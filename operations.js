function asignate(details,boxes,info){    
    const boxEco = boxes.find(box => box.id === 5);
    console.log('información de la caja ecorefilliable '+ boxEco);
    const boxStd = boxes.find(box => box.id === 4);
    let managementBox= boxStd;
  
    let is_web= details[0].tipo ===0?false:true;

  console.log('Se necesita saber si is_web es web ', is_web);
   // let order_type= details.

    //console.log(details);
    let sumW =0;
    let sumV= 0;
    let sumEcoRefill= 0;
        console.log('se inicio el cubicaje: peso:'+sumW+' volumen: '+sumV);
                  
        //  console.log('No hay ecoRefill suficientes para hacer una caja ecorefill');
        let cubic = []; 
        var counter=0;
        cubic.push({
            type:4,
            sku:[]
        });
        let flag =true;
        let deta = details.map(detail=>{       
            
            if(detail.ecorefill===1){
                sumEcoRefill++;                                             
                if (parseFloat(sumEcoRefill)>4){
                    console.log('Ecorefilliable  y hay '+sumEcoRefill +'y deben haber '+boxEco.min);
                    console.log('se conviertio la caja en Ecorefilliable');
                    cubic[counter].type=5;
                    managementBox=boxEco;
                  
                }else{
                    console.log('lo otro' +sumEcoRefill);
                }
            }
            console.log('info gral prodicto',detail);
           // if(flag){
                sumW+=parseFloat(detail.weight);
                sumV+=parseFloat(detail.volume);
             //   flag=false;
            //}
            console.log('volumenAcu: '+sumV+ ' <= volumeCajita: '+managementBox.volume + '||  pesoAcu: '+sumW+' pesoCajita: '+managementBox.capacity);
            if (parseFloat(sumV)<=parseFloat(managementBox.volume) && parseFloat(sumW)<=parseFloat(managementBox.capacity)){
                //Se inserta en la caja y se continúa normal con el proceso                
                const exists = cubic[counter].sku.find(x=>x.id===detail.id);
                if(exists){                
                    exists.quantity++;                    
                }else{                    
                    cubic[counter].sku.push(detail);                    
                }                               
            }else{
                //Se realiza el cierre de caja y se abre una nueva con los skus elegidos
                sumW=sumV=0;
                sumEcoRefill=0;
                cubic.push({
                    type:4,
                    sku:[]
                });
                managementBox=boxStd;
                counter++;
                cubic[counter].sku.push(detail);

            }         
         });
        // Este método permite redimensionar la última caja, solo se debe utilizar para las web.
       if (is_web){
            cubic[cubic.length-1].type= resize(sumW,sumV,boxes);
            console.log('cantidad de cajitas: ',cubic.length);
       }else{
        console.log('-------------------La caja se esta redimensionando-----------------');
       }


const testing = cubic.map(box=>{

    console.log('cantidad de skus distintos por cajita: ',box.sku.length);
        const testing2 = box.sku.map(prueba=>{
            console.log('order_id: ' + prueba.id +' cantidad: '+prueba.quantity +'location'+ prueba.location );
        });
})
return cubic;
    
}

function resize(sumW,sumV,boxes){

    console.log('-------------------La caja se esta redimensionando-----------------');
    let flag=true;
    let boxtype=5;

    boxes =boxes.filter(X=>X.web===1);

    const prueba = boxes.map(cajas=>{
    console.log('volumenAcu: '+sumV+ ' <= volumeCajita: '+cajas.volume + '||  pesoAcu: '+sumW+' pesoCajita: '+cajas.capacity);
    if(parseFloat(sumV)<=parseFloat(cajas.volume) && parseFloat(sumW)<=parseFloat(cajas.capacity)){
        
        if(flag){
            console.log('Se cambio la caja final a: '+cajas.id);
            boxtype= cajas.id;
            flag=false;
        }
        
    }
});
return boxtype;
}
function Unidades(num){

    switch(num)
    {
        case 1: return 'UN';
        case 2: return 'DOS';
        case 3: return 'TRES';
        case 4: return 'CUATRO';
        case 5: return 'CINCO';
        case 6: return 'SEIS';
        case 7: return 'SIETE';
        case 8: return 'OCHO';
        case 9: return 'NUEVE';
    }
    return '';
}

function Decenas(num){

    let decena = Math.floor(num/10);
    let unidad = num - (decena * 10);

    switch(decena)
    {
        case 1:
            switch(unidad)
            {
                case 0: return 'DIEZ';
                case 1: return 'ONCE';
                case 2: return 'DOCE';
                case 3: return 'TRECE';
                case 4: return 'CATORCE';
                case 5: return 'QUINCE';
                default: return 'DIECI' + Unidades(unidad);
            }
        case 2:
            switch(unidad)
            {
                case 0: return 'VEINTE';
                default: return 'VEINTI' + Unidades(unidad);
            }
        case 3: return DecenasY('TREINTA', unidad);
        case 4: return DecenasY('CUARENTA', unidad);
        case 5: return DecenasY('CINCUENTA', unidad);
        case 6: return DecenasY('SESENTA', unidad);
        case 7: return DecenasY('SETENTA', unidad);
        case 8: return DecenasY('OCHENTA', unidad);
        case 9: return DecenasY('NOVENTA', unidad);
        case 0: return Unidades(unidad);
    }
}

function DecenasY(strSin, numUnidades) {
    if (numUnidades > 0)
    return strSin + ' Y ' + Unidades(numUnidades)

    return strSin;
}

function Centenas(num) {
    let centenas = Math.floor(num / 100);
    let decenas = num - (centenas * 100);

    switch(centenas)
    {
        case 1:
            if (decenas > 0)
                return 'CIENTO ' + Decenas(decenas);
            return 'CIEN';
        case 2: return 'DOSCIENTOS ' + Decenas(decenas);
        case 3: return 'TRESCIENTOS ' + Decenas(decenas);
        case 4: return 'CUATROCIENTOS ' + Decenas(decenas);
        case 5: return 'QUINIENTOS ' + Decenas(decenas);
        case 6: return 'SEISCIENTOS ' + Decenas(decenas);
        case 7: return 'SETECIENTOS ' + Decenas(decenas);
        case 8: return 'OCHOCIENTOS ' + Decenas(decenas);
        case 9: return 'NOVECIENTOS ' + Decenas(decenas);
    }

    return Decenas(decenas);
}

function Seccion(num, divisor, strSingular, strPlural) {
    let cientos = Math.floor(num / divisor)
    let resto = num - (cientos * divisor)

    letras = '';

    if (cientos > 0)
        if (cientos > 1)
            letras = Centenas(cientos) + ' ' + strPlural;
        else
            letras = strSingular;

    if (resto > 0)
        letras += '';

    return letras;
}

function Miles(num) {
    let divisor = 1000;
    let cientos = Math.floor(num / divisor)
    let resto = num - (cientos * divisor)

    let strMiles = Seccion(num, divisor, 'UN MIL', 'MIL');
    let strCentenas = Centenas(resto);

    if(strMiles == '')
        return strCentenas;

    return strMiles + ' ' + strCentenas;
}

function Millones(num) {
    console.log('aqui esta la info ', num)

    let divisor = 1000000;
    let cientos = Math.floor(num / divisor)
    let resto = num - (cientos * divisor)

  
    console.log(resto);
    if(resto>0)
        strMillones = Seccion(num, divisor, 'UN MILLON', 'MILLONES');
    else
        strMillones = Seccion(num, divisor, 'UN MILLON DE', 'MILLONES DE');

    strMiles = Miles(resto);

    if(strMillones == '')
        return strMiles;

    return strMillones + ' ' + strMiles;
}

function numberTowords(num) {
    let data = {
        numero: num,
        enteros: Math.floor(num),
        centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
        letrasCentavos: '',
        letrasMonedaPlural: 'Pesos',
        letrasMonedaSingular: 'Peso', 
        letrasMonedaCentavoPlural: 'CENTAVOS',
        letrasMonedaCentavoSingular: 'CENTAVO'
    };

    if(data.enteros == 0){
        return 'CERO ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
    }        
    if (data.enteros == 1){
        return Millones(data.enteros) + ' ' + data.letrasMonedaSingular + ' ' + data.letrasCentavos;
    }        
    else{       
        return Millones(data.enteros) + ' ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
    }    
}

module.exports.asignate = asignate;
module.exports.numberTowords = numberTowords;
