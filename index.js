const express = require('express');
const conecction = require('./connection')
const operations = require('./operations')
const app = express();

app.use(express.json());


app.get('/',(req,res)=>{
    conecction.conectar();

    res.send('One Consultants API');
});

app.post('/api/provenzal/forecast/',async(req,res)=>{

    console.log(req.body)
    let info ={
        references:req.body.references,
        shop:req.body.shop
    }
       // let survey =  await conecction.getInfoReferences(info)  


        var d = new Date();
        var n1 = d.getMonth();
        var y1 = d.getFullYear();
        var n2 = d.getMonth()===11 ?1:d.getMonth()+2;
        var y2 = d.getMonth()===11? d.getFullYear()+1:y1;
        var n3 = d.getMonth()===11?2:d.getMonth()+3;
        var y3 = d.getMonth()===11? d.getFullYear()+1:y1;

        console.log(y3)

        console.log(n3)


        let references = info.references.map(information=>{         

            let dias = []
                for (let i=0; i<=59;i++){
                    
                    dias[i]={
                        day:i,
                        forecast:Math.round(Math.random() * (121 - 11) )
                    }
                } 
                return    {
                    forecast:dias,
                    reference:information.reference                   
                }                   
        })
console.log(references)
       
            res.send({
            references
        });   
         
});

app.get('/api/provenzal/cubicador/:order_id',(req,res)=>{
  
    /*const { error } = validateInfo(req.params.order_id);        

    if (error){
    //400 bad request
        return res.status(400).send(error.details[0].message);
    }*/

    //conecction.conectar(); 
    const status =conecction.exists(req.params.order_id)
    .then(exist=>{

  if(exist==0)
 { 
    const boxes= conecction.getBoxes()
    .then(box =>{
        const info= conecction.totalWeightVolume(req.params.order_id)
        .then(inf =>{

            const detail= conecction.findOrder(req.params.order_id,1)
            .then(deta =>{
                const cubic= operations.asignate(deta,box,inf);
                if(cubic.length>0)
                {                    
                    const saved= conecction.saveCubic(cubic,req.params.order_id)
                .then(save =>{
                    console.log(save)
                    res.send({
                        message:'Se cubico correctamente',
                        status:'ok'
                    });
                }
                ).catch(err=> console.log(err.message));
                }else{
                    console.log('no hay cubicaje para almacenar');
                }
                

            }).catch(err=> console.log(err.message));        
        }).catch(err=> console.log(err.message));
        
    
    
    })
    .catch(err=> console.log(err.message));
}
else{
    if(exist==2)
    res.send({
        message:'Esa orden ya fue cubicada v3.0',
        status:'nok'
    });
    if(exist==1)
    res.send({
        message:'Esa orden no existe v3.0',
        status:'nok'
    });
}
})
.catch(err=> console.log(err.message));

return {
    message:'Se cubico exitosamente la orden',
    status:'ok'
};
});

app.get('/api/numbers/:plata',(req,res)=>{
 let value = operations.numberTowords(req.params.plata);

 res.send({
    message:value,
    status:'ok'
});
   console.log(value);
});

function validateInfo(info){
    
    const schema = {
        order_id: Joi.number().required()
    };
    let validate=Joi.validate(info,schema);
    console.log(validate);
    return(validate);
}


//PORT
const port = process.env.PORT || 3000;

app.listen(port, ()=> console.log('listening on port '+port+''));

