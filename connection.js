const mysql = require('mysql2/promise');
const mySqlserver ='sqltesting.cdyj8qpiv1rx.us-east-2.rds.amazonaws.com';
const user='mysqlusertesting'
const password='123456789'
const database='inventario_bodegas_provenzal'
const pool = require('./connect');

//no tenemos información del descuento de venta del producto (depende del pos)

function getInfoReferences (info) {
   return new Promise((resolve,reject)=>{
   pool.getConnection(function(err, connection){
    if(err){
        console.log('error en la conexión', err);
        
        resolve({
            error:'Error en la conexión',
            msg:err
        })
    } 
   referenceQueries = '\''+info.references.map(x => x.reference).join('\',\'') +'\''
   console.log(referenceQueries) 

    //console.log(info.references.reference.join(','));
    
/*
    connection.query("SELECT referencia, line,category, price FROM `products` where referencia in (?)"            
    ,[references], function(err, data){     
       
      
      let existentes = data.map((product)=>{

         
      })
        let returned_value=false
        let employees= data.map(token=>{
        console.log(token)
        returned_value=true 
        })                
        connection.release()
        console.log(returned_value)
        
    });*/
    resolve(info.references.reference.join(','))
});     
});       
}



 async function getBoxes () {
   const conn = await mysql.createConnection({
      host: mySqlserver,
      user: user,
      password: password,
      database: database,
      port: 3306
   });
   

   let cajas= await conn.execute('SELECT weight_capacity*fully_percentaje/100 capacidad,web, (deep*fully_percentaje/100) deep, high,width,id, minQuantity FROM `boxes` where id in (4,5,6,7) order by deep* high* width asc', []);

   let box= cajas[0].map(caja=>{
      return {
         capacity:caja.capacidad,
         id:caja.id,
         volume:caja.deep*caja.width*caja.high,
         min:caja.minQuantity,
         web:caja.web
      }
   });
   conn.end();   
   return box ;
 }



 
 async function totalWeightVolume (orderId) {
   const conn = await mysql.createConnection({
      host: mySqlserver,
      user: user,
      password: password,
      database: database,
      port: 3306
   });

   let data= await conn.execute('SELECT sum(pro.width*pro.deep*pro.high) volume, sum(pro.ecorefill) ecoQuantity, sum(pro.weight) weight, sum(od.quantity) quantity FROM orders o inner join order_details od on od.order_id = o.id inner join products pro on pro.id=od.product_id where o.id =? group by o.id', [orderId]);

      const info ={
         ecoQuantity:data[0][0].ecoQuantity,
         volume:data[0][0].volume,
         weight:data[0][0].weight,
         quantity:data[0][0].quantity,
         status:true
      };
      conn.end();  
      return info;
 }

 async function saveCubic (cubic,order) {
<<<<<<< HEAD

   console.log('el valor del id de la orden es:', order);
=======
>>>>>>> 614bb0e8e8830be3f67d1a4b5366de26a0cc970f
  
   const mysql = require('mysql');
   try{
          
   const conn = await mysql.createConnection({
      host: mySqlserver,
      user: user,
      password: password,
      database: database,
      port: 3306
   });

const vble = cubic.map(cub=>{
   console.log('Información a insertar: caja, data,tipo y orden '+cub.type+', 1,p,'+order+'');
   var query = conn.query('INSERT INTO `packets`( `box_id`, `data`, `type`,`order_id`,`version`) VALUES (?,?,?,?,1)', [cub.type,1,'P',order],
   function(err, result) {
      const iterable = cub.sku.map(sku=>{
        var query2 = conn.query('INSERT INTO `packet_details`(`order_detail_id`, `quantity`, `packet_id`) VALUES (?,?,?)', [sku.id,sku.quantity,result.insertId],
        function(err, result2) {
        });
    });
   });
})
    }catch(err){
       console.log(err.message);
    }
<<<<<<< HEAD
=======

    setTimeout(function(){
      console.log("Wait for it...");
  }, 4000);


>>>>>>> 614bb0e8e8830be3f67d1a4b5366de26a0cc970f
      return 'ok';
}

 async function findOrder (orderId,option) {
   let productos= [];
   console.log('orderID: ',orderId);
   const mysql = require('mysql2/promise');
   const conn = await mysql.createConnection({
      host: mySqlserver,
      user: user,
      password: password,
      database: database,
      port: 3306
   });

   let data="";
   if(option===0){
       data= await conn.execute('SELECT pro.id,pro.width, pro.weight,pro.enabled_above,pro.deep,pro.high,pro.lone, per.person, '
         +'pro.ecorefill,od.id orderdetail_id, od.quantity FROM orders o inner join clients  per on per.id=o.client_id inner join order_details od on od.order_id = '
         +' o.id inner join products pro on pro.id=od.product_id where o.id =? order by pro.ecorefill DESC, '
         +' pro.weight desc, (pro.width*pro.deep*pro.high) desc, od.quantity desc',
         [orderId]);

   }else{
       data= await conn.execute('SELECT pro.id,pro.width, l.nombre location, pro.weight,pro.enabled_above,pro.deep,pro.high,pro.lone, per.person,    pro.ecorefill,od.id orderdetail_id, od.quantity FROM orders o inner join clients  per on per.id=o.client_id inner join order_details od on od.order_id =     o.id inner join products pro on pro.id=od.product_id inner join inventories i on i.product_id=od.product_id inner join locations l on l.id=i.location_id and l.warehouse_id=11  where o.id =? order by l.print_order asc',     [orderId]);

   }
    let box= data[0].map(detail=>{
      for(i=0; i<detail.quantity;i++){
         productos.push({
            weight:detail.weight,
            id:detail.orderdetail_id,
            volume:detail.width*detail.high*detail.deep,
            packet:null,
            boxtype:null,
            quantity:1,
            ecorefill:detail.ecorefill,
            tipo:detail.person
           }) ;
      }
  });
  conn.end();  
   return productos;

}


async function exists (orderId){
   //no existe esa orden
   let flag=1;
   const conn = await mysql.createConnection({
      host: mySqlserver,
      user: user,
      password: password,
      database: database,
      port: 3306
   });

   let data= await conn.execute('select o.id id,order_id oid from orders o left join packets pa on pa.order_id=o.id where o.id = ?',
    [orderId]);
    let box= data[0].map(detail=>{
       //Existe la orden, pero esta asignada
       flag=2;
          if(detail.oid==null)
          //Existe la orden y se puede asignar
            flag=0;
   });
   conn.end();  
   return flag;

}


function cerrarConeccion(){
     console.log('Se cerro exitosamente la conexión');
}

module.exports.getInfoReferences = getInfoReferences;
module.exports.exists = exists;
 module.exports.saveCubic = saveCubic;
 module.exports.totalWeightVolume = totalWeightVolume;
 module.exports.getBoxes = getBoxes; 
 module.exports.cerrarConeccion = cerrarConeccion;
 module.exports.findOrder = findOrder;
